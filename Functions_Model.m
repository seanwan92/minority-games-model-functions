function [Id] = Index2(P)
% Index2 generates the history index of the game by taking the probabilty
% of one of the possible patterns.
I = randperm(P);
Id = I(1);
end

function [a,st] = MG_Orig3(N,t,a,st,S,U,A,Id)
% This function gives the output of the chosen strategy per agent
% Agents will choose their strategy based on the score with the highest
% amount of virtual points that have been tallied each round.
for i=1:N
    if t==1
        I_i = randperm(S);
        st(t,i)=I_i(1);
        a(t,i)=A(Id,i,st(t,i));
    else
        % Run through each strategy table and select the best table based
        % on past outcomes
        for s=1:S
            Up(s)=U(s,i,t);
        end
        [y,I]=max(Up);
        st(t,i)=I;                              % Chosen strategy from strategy table.
        a(t,i)=A(Id,i,st(t,i));                 % Chosen action (1 or -1) extracted from the history index, and strategy table per agent
    end
end
end

function [As,H,Ht] = Attendance(As,t,a,N,H,M)
% Attendance - Determines the majority and minority outcomes
As(t)=sum(a(t,1:N));                            % Number this generates indicates the majority action of the game
% Anyone with the opposite sign of As(t) won.
H(t+M)=-sign(As(t));                            % New History of who was in the minority
Ht=H(t+M);                                      % adding the minority outcome to the history index
end

function [U,DC,C] = Payoff_Orig(t,N,S,A,Id,st,U,DC,Ht,a,C)
% This function notifies the agent if they won or lost that round. If they won, the agent adds
% a virtual point to the score they used to win
for i=1:N
    for s=1:S
        u = Ht*A(Id,i,s);                      % Each strategy score's payoff from current round
        if s==st(t,i)                          % To add the virtual point to a winning strategy
            U(s,i,t+1) = U(s,i,t)+u;           % Cumulative payoff from all rounds
            C(t+1,i)=C(t,i)+u;
        end
        DC(t+1,i)=Ht*a(t,i);                   % Determines if you win/lose
    end
end
end

function [a,st] = MG_Thermal(N,t,a,st,S,U,A,Id,gamma)
% This function gives the output of the chosen strategy per agent
% Agents will choose their strategy based on the score with the highest
% amount of virtual points that have been tallied each round.
for i=1:N
    if t==1
        I_i = randperm(S);
        st(t,i)=I_i(1);
        a(t,i)=A(Id,i,st(t,i));
    else
        % Run through each strategy table and select the best table based
        % on past outcomes
        K = exp(gamma*U(1,i,t))+exp(gamma*U(2,i,t));
        Pr1=exp(gamma*U(1,i,t))/K;              % Pr of st(1)
        Pr2=exp(gamma*U(2,i,t))/K;              % Pr of st(2)
        % Choosing Strategy
        st_ran = rand;
        if st_ran <=Pr1
            st(t,i)=1;
        else
            st(t,i)=2;
        end
        % Chosen strategy from strategy table.
        a(t,i)=A(Id,i,st(t,i));                 % Chosen action
    end
end
end

function [a,st,a_hold] = MG_Hold3(N,t,a_hold,a,st,S,U,A,Id)
% MG_Hold3 describes how each agent chooses their action for the upcoming
% round they are facing. The behaviour of an agent is if their strategy
% worked, the agent will choose the same strategy they used last round. If
% they had a negative payoff in the previous round, the agent then chooses
% a new strategy on random.
for i=1:N
    if a_hold(t,i) ~= 0                          % If the upcoming round is the 2nd half of a round trip transaction
        a(t,i) = a_hold(t,i);                    % Action needs to be held.
        st(t,i) = st(t-1,i);                     % Strategy needs to be held.
    elseif t==1
        I_i = randperm(S);                       % Random Strategy Choice
        st(t,i)=I_i(1);
        a(t,i)=A(Id,i,st(t,i));
        a(t+1,i)=-sign(a(t,i));                  % Set up opposite action for next round
        a_hold(t+1,i) = a(t+1,i);                % Holds opposite action for next round
    else
        for s=1:S
            Up(s)=U(s,i,t);
        end
        [y,I]=max(Up);                          % Searches for best strategy table
        st(t,i)=I;                              % Chosen strategy from strategy table.
        a(t,i)=A(Id,i,st(t,i));                 % Chosen action (1 or -1) extracted from the history index, and strategy table per agent
        a(t+1,i)=-sign(a(t,i));                 % Set up opposite action for next round
        a_hold(t+1,i) = a(t+1,i);               % Holds opposite action for next round
    end
end
end

function [p] = Price(t,p,p_i,N,As)
% Calculates the share price at each round
if t-1==0
    p(1,t) = p_i;                                   % seeds initial price of stock as an positive value greater than 0
else
    p(1,t) = p(1,t-1)*((N+As(t))/(N-As(t)));        % calculates new price
end
end

function [U,DC] = Payoff2(t,N,S,st,U,DC,Ht,a)
% This function notifies the agent if they won or lost that round. If they won, the agent adds
% a virtual point to the score they used to win, but only in every 2nd
% round.
if mod(t,2)==0                                     % only calculate payoffs @ even periods
    for i=1:N
        for s=1:S
            u = Ht*a(t,i);                         % Each strategy score's payoff from current round
            if s==st(t,i)                          % To add the virtual point to a winning strategy
                U(s,i,t+1) = U(s,i,t)+u;           % Cumulative payoff from all rounds
            end
            DC(t+1,i)=Ht*a(t,i);                   % Determines if you win/lose
        end
    end
else
    for i=1:N
        for s=1:S
            U(s,i,t+1)= U(s,i,t);                  % Doesn't calculate payoff b/c not required.
        end
    end
end
end

function [C,Sh,Capital] = Capital2(Capital,t,tf,N,a,C,p,p_i,Sh)
% Calculates the Capital of the agents
for i = 1:N
    if t == 1                                           % Different action if t = 1
        if a(t,i)>= 1                                   % checks loop for initial time step to establish correct cash and shares for R2.
            % If agent's strategy is to buy then sell
            C(t+1,i) = C(t,i)-p(t);                     % Deducts cash in time 2 b/c I bought
            Sh(t+1,i)= Sh(t,i)+1/p(t);                  % Increases amount of shares b/c I bought
            C(t+2,i) = C(t+1,i)+ p(t+1)/p(t);           % Calculates Cash
            Sh(t+2,i) = Sh(t,i);                        % Calculates Shares
            Capital(t+2,i) = C(t+2,i)+Sh(t+2,i)*p(tf);  % Calculates Capital
        else
            % If agent's strategy is to sell then buy
            C(t+1,i)= C(t)+p(t+1)/p(t);                 % Cash increases by amount of sale
            Sh(t+1,i) = Sh(t)-1/p(t);                   % Shares decrease by price sold by
            C(t+2,i) = C(t+1,i) + p(t)/p_i;             % Calculates Cash
            Sh(t+2,i) = Sh(t+1,i) + (1/p(t+1));         % Calculates Shares
            Capital(t+2,i) = C(t+2,i)+Sh(t+2,i)*p(tf);  % Calculates Capital
        end
    elseif a(t,i)>= 1
        % If agent's strategy is to buy then sell
        C(t+2,i) = C(t,i)-1 + p(t+1)/p(t);              % Calculates Cash
        Sh(t+2,i) = Sh(t,i);                            % Calculates Shares
        Capital(t+2,i) = C(t+2,i)+Sh(t+2,i)*p(tf);      % Calculates Capital
    else
        % If agent's strategy is to sell then buy
        C(t+2,i) = C(t,i)-1 + p(t)/p(t-1);              % Calculates Cash
        Sh(t+2,i) = Sh(t,i)- (1/p(t-1))+(1/p(t+1));     % Calculates Shares
        Capital(t+2,i) = C(t+2,i)+Sh(t+2,i)*p(tf);      % Calculates Capital
    end
end
end

function [maj_count,min_count] = MarketTiming(N,tf,a,As)
% Counts whoe made the minority the most times
% Initialising minority and majority counts
maj_count = zeros(1,N);                         % majority count
min_count = zeros(1,N);                         % minority count
for t=1:tf
    for i=1:N
        if sign(a(t,i)) == sign(As(t))          % checks the sign of each agents strategy against attendance
            maj_count(i) = maj_count(i) + 1;    % if the same, add to majority count
        else
            min_count(i) = min_count(i) + 1;    % else add to minority count
        end
    end
end
end






